import request from '@/utils/request'

export function fetchList(data) {
  return request({
    url: '/api/user/list',
    method: 'post',
    params:data
  })
}

export function createRow(data) {
  return request({
    url: '/api/user/add',
    method: 'post',
    data
  })
}

export function updateRow(data) {
  return request({
    url: '/api/user/update',
    method: 'post',
    data
  })
}
export function deleteRow(data) {
  return request({
    url: '/api/user/delete',
    method: 'post',
    params:data
  })
}
